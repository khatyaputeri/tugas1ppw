from django.db import models

# Create your models here.

class DataProfile(models.Model):
	nama = models.CharField(max_length = 50)
	birthday = models.CharField(max_length = 50)
	gender = models.CharField(max_length = 50)
	expertise = models.TextField(max_length = 50)
	description = models.CharField(max_length = 50)
	email = models.CharField(max_length = 50)
	
