from django.apps import AppConfig


class HalprofilConfig(AppConfig):
    name = 'halprofil'
