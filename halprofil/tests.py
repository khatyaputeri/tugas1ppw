from django.test import TestCase, Client
from django.urls import resolve
from .views import index, profile

# Create your tests here.
class HalProfilTest(TestCase):
	def test_profile_url_is_exist(self):
	    response = Client().get('/profile/')
	    self.assertEqual(response.status_code,200)

	def test_profile_is_using_halprofil_template(self):
	    response = Client().get('/profile/')
	    self.assertTemplateUsed(response, 'halprofil/hal_profil.html')

	def test_profile_using_index_func(self):
	    found = resolve('/profile/')
	    self.assertEqual(found.func, index)

	def test_profile_is_not_none(self):
	    self.assertIsNotNone(profile.nama)
	    self.assertIsNotNone(profile.birthday)
	    self.assertIsNotNone(profile.gender)
	    self.assertIsNotNone(profile.expertise)
	    self.assertIsNotNone(profile.description)
	    self.assertIsNotNone(profile.email)
        
